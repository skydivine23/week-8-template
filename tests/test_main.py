from unittest.mock import Mock

from csuibot.main import help, zodiac, shio, send_welcome, default


def test_help(mocker):
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mock_message = Mock()

    help(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (
        'skydivine_bot v2.1.3\n\n'
        'Dari Rakyat , oleh Pemerintah, untuk Wilayah!'
    )
    assert args[1] == expected_text


def test_zodiac(mocker):
    fake_zodiac = 'foo bar'
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.lookup_zodiac', return_value=fake_zodiac)
    mock_message = Mock(text='/zodiac 2015-05-05')

    zodiac(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == fake_zodiac


def test_zodiac_invalid_month_or_day(mocker):
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.lookup_zodiac', side_effect=ValueError)
    mock_message = Mock(text='/zodiac 2015-25-05')

    zodiac(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Month or day is invalid'


def test_shio(mocker):
    fake_shio = 'foo bar'
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.lookup_chinese_zodiac', return_value=fake_shio)
    mock_message = Mock(text='/shio 2015-05-05')

    shio(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == fake_shio


def test_shio_invalid_year(mocker):
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.lookup_chinese_zodiac', side_effect=ValueError)
    mock_message = Mock(text='/shio 1134-05-05')

    shio(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Year is invalid'


def test_send_welcome(mocker):
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.lookup_chinese_zodiac', side_effect=ValueError)
    mock_message = Mock(text='/start')

    send_welcome(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = ("Hi there, I am ZoShio-Kabbalah bot.\n"
                     "Please insert a command with a date and I'll find out his/her \n"
                     "for zodiac . Example: /zodiac yyyy-mm-dd \n"
                     "for shio .  Example: /shio yyyy-mm-dd\n"
                     "warning : 3 shio terakhir tidak ada")
    assert args[1] == expected_text


def test_invalid_command(mocker):
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.default')
    mock_message = Mock(text='Hello world')

    default(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == "Command is invalid"
