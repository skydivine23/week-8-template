from os import environ

token = "365394658:AAG3wDK99DsKM2q9qX-F2U4RKc9mNUjEGa4"
APP_ENV = environ.get('APP_ENV', 'development')
DEBUG = environ.get('DEBUG', 'true') == 'true'
TELEGRAM_BOT_TOKEN = environ.get('TELEGRAM_BOT_TOKEN', token)
LOG_LEVEL = environ.get('LOG_LEVEL', 'DEBUG')
WEBHOOK_HOST = environ.get('WEBHOOK_HOST', 'https://zoshio-kabbalah.herokuapp.com/bot')
WEBHOOK_URL = environ.get('WEBHOOK_URL', 'https://zoshio-kabbalah.herokuapp.com/')
